﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Reservation
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
        [DataType(DataType.Date)]
        public DateTime ReservationFrom { get; set; }
        [DataType(DataType.Date)]
        public DateTime ReservationTo { get; set; }

    }
}
