﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Invoice
    {
        public int Id { get; set; }
        [Required]
        public double PriceTotal { get; set; }
        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public List<Reservation> Reservations { get; set; }
        public DateTime Issued { get; set; }

    }
}
