﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Room
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string RoomNumber { get; set; }
        [Required]
        public int PricePerNight { get; set; }


        public List<Reservation> Reservations { get; set; } = new List<Reservation>();

        public int HotelId { get; set; }
        public Hotel Hotel { get; set; }
    }
}
