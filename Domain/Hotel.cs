﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class Hotel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        [MaxLength(200)]
        public string StreetAddress { get; set; }
        public List<Room> Rooms { get; set; } = new List<Room>();

        public int CityId { get; set; }
        public City City { get; set; }
    }
}
