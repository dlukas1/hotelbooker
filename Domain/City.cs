﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain
{
    public class City
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        public List<Hotel> Hotels { get; set; } = new List<Hotel>();
        public int CountryId { get; set; }
        public Country Country { get; set; }
    }
}
