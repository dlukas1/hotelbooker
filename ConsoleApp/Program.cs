﻿using DAL;
using DAL.Repositories;
using System;


namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("EF Test");
            using(var ctx = new AppDbContext())
            {
                //SeedData.Initialize(ctx);
                var repo = new HotelRepo(ctx);
                foreach (var hotel in repo.All)
                {
                    Console.WriteLine(hotel.Name);
                }
                Console.ReadLine();
            }
        }
    }
}
