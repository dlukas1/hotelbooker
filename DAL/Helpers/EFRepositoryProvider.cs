﻿using DAL.Repositories;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Helpers
{
    public class EFRepositoryProvider : IRepositoryProvider
    {
        private readonly IDataContext _dataContext;
        public EFRepositoryProvider(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        private readonly Dictionary<Type, object> RepositoryCache
             = new Dictionary<Type, object>();

        private static readonly Dictionary<Type, Func<IDataContext, object>> DictionaryOfRepoCreationFuncs
            = new Dictionary<Type, Func<IDataContext, object>>()
            {
                    { typeof(ICountryRepo),dataContext => new CountryRepo(dbContext : dataContext)},
                    { typeof(ICityRepo),dataContext => new CityRepo(dbContext : dataContext)},
                    { typeof(ICountryRepo),dataContext => new CountryRepo(dbContext : dataContext)},
                    { typeof(IHotelRepo),dataContext => new HotelRepo(dbContext : dataContext)},
                    { typeof(RoomRepo),dataContext => new RoomRepo(dbContext : dataContext)},
                    { typeof(ReservationRepo),dataContext => new ReservationRepo(dbContext : dataContext)},
                    { typeof(IInvoiceRepo),dataContext => new InvoiceRepo(dbContext : dataContext)},
                    { typeof(IUserRepo),dataContext => new UserRepo(dbContext : dataContext)}
            };




        public TRepository GetCustomRepository<TRepository>()
            => GetOrMakeRepository<TRepository>();

       

        private TRepository GetOrMakeRepository<TRepository>
            (Func<IDataContext, object> repoCreationFunction = null)
        {
            object repo;
            RepositoryCache.TryGetValue(typeof(TRepository), value: out repo);
            if (repo != null)
            {   //if repo exist return it
                return (TRepository)repo;
            }
            if (repoCreationFunction == null)
            {
                DictionaryOfRepoCreationFuncs.
                    TryGetValue(typeof(TRepository), out repoCreationFunction);
            }
            if (repoCreationFunction == null)
            {
                throw new ArgumentNullException($"No factory found for repository type {typeof(TRepository).FullName}");
            }
            //create a repo
            repo = repoCreationFunction(arg: _dataContext);
            //save repo to cache
            RepositoryCache[key: typeof(TRepository)] = repo;
            //return repo
            return (TRepository)repo;
        }

        public IRepository<TEntity> GetStandertRepository<TEntity>() where TEntity : class
        {
            return GetOrMakeRepository<IRepository<TEntity>>(dataContext => new EFRepository<TEntity>(dataContext));
        }
    }
}
