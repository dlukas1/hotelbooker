﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class RoomRepo : EFRepository<Room>, IRoomRepo
    {
        public RoomRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
