﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class ReservationRepo : EFRepository<Reservation>, IReservationRepo
    {
        public ReservationRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
