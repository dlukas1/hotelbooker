﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class HotelRepo :EFRepository<Hotel>, IHotelRepo
    {
        public HotelRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
