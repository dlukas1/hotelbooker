﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;


namespace DAL.Repositories
{
    public class CityRepo : EFRepository<City>, ICityRepo
    {
        public CityRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
