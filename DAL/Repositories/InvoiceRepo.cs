﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;


namespace DAL.Repositories
{
    public class InvoiceRepo : EFRepository<Invoice>, IInvoiceRepo
    {
        public InvoiceRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
