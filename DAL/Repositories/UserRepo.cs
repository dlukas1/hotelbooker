﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class UserRepo : EFRepository<User>, IUserRepo
    {
        public UserRepo (IDataContext dbContext) : base(dbContext) { }
    }
}
