﻿using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class EFRepository<TEntityType> : IRepository<TEntityType>
        where TEntityType : class
    {
        protected DbContext RepoDbContext;
        protected DbSet<TEntityType> RepoDbSet;

        public EFRepository(IDataContext dataContext)
        {
            RepoDbContext = dataContext as DbContext??
                throw new ArgumentNullException("No EF DB context");
            RepoDbSet = RepoDbContext.Set<TEntityType>();
        }

        //Standart methods
        public List<TEntityType> All
        {
            //take all records in DbSet and return them
            get { return RepoDbSet.ToList(); }
        }
        //Async methods
        public async Task<IEnumerable<TEntityType>> AllAsync() =>
            await RepoDbSet.ToListAsync();

        public void Add(TEntityType entity)
        {
            RepoDbSet.Add(entity);
        }
        public async Task AddAsync(TEntityType entity)
        {
            if (entity == null)
                throw new InvalidOperationException(message: "Can't add null");
            await RepoDbSet.AddAsync(entity: entity);
        }

        public void Delete(int id)
        {
            var entity = GetById(id);
            RepoDbSet.Remove(entity);
        }

        public void Delete(TEntityType entity)
        {
            RepoDbSet.Remove(entity);
        }

        public TEntityType GetById(int id)
            => RepoDbSet.Find(id);
        public Task<TEntityType> GetByIdAsync(params object[] id) =>
             RepoDbSet.FindAsync(keyValues:id);

        public int SaveChanges()
            => RepoDbContext.SaveChanges();

        public void Update(TEntityType entity)
        {
            RepoDbSet.Attach(entity);
        }
    }
}
