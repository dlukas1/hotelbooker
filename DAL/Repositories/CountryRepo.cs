﻿using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class CountryRepo : EFRepository<Country>, ICountryRepo
    {
        public CountryRepo(IDataContext dbContext) : base(dbContext) { }
    }
}
