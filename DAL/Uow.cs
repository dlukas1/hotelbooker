﻿using DAL.Repositories;
using Domain;
using Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public class Uow : IUow
    {
        private readonly IDataContext _dataContext;
        private readonly IRepositoryProvider _repositoryProvider;
        public Uow(IDataContext dataContext, IRepositoryProvider repositoryProvider)
        {
            _repositoryProvider = repositoryProvider;
            _dataContext = dataContext ??
                throw new ArgumentNullException(nameof(dataContext));
        }

        public IInvoiceRepo Invoices => new InvoiceRepo(_dataContext);

        public IReservationRepo Reservations => new ReservationRepo(_dataContext);

        public IRepository<Country> Countries => new EFRepository<Country>(_dataContext);

        public IRepository<City> Cities => new EFRepository<City>(_dataContext);

        public IRepository<Hotel> Hotels => new EFRepository<Hotel>(_dataContext);

        public IRepository<Room> Rooms => new EFRepository<Room>(_dataContext);

        public IRepository<User> Users => new EFRepository<User>(_dataContext);

        public int SaveChanges()
            => ((DbContext)_dataContext).SaveChanges();

        
    }
}
