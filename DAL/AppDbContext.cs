﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Interfaces;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;


namespace DAL
{
    public class AppDbContext : IdentityDbContext<ApplicationUser>, IDataContext
    {
        public AppDbContext() : base() { }
        public DbSet<Domain.Country> Countries { get; set; }
        public DbSet<Domain.City> Cities { get; set; }
        public DbSet<Domain.Hotel> Hotels { get; set; }
        public DbSet<Domain.Room> Rooms { get; set; }
        //public DbSet<User> Users { get; set; }
        public DbSet<Domain.Reservation> Reservations { get; set; }
        public DbSet<Domain.Invoice> Invoices { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server = (localdb)\mssqllocaldb; 
            Database = HotelBookerDb; Trusted_Connection = True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Prevent cascade delete
            foreach (var relationship in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
                //now when trying to remove any entity will get an exception
                //it’s possible only if you manually delete all related entries
            }

            base.OnModelCreating(modelBuilder);

        }
    }
}
