﻿using Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAL
{
    public class SeedData
    {
        public static void Initialize(AppDbContext ctx)
        {
            if (!ctx.Hotels.Any())
            {
                Country Estonia = new Country { Name = "Estonia" };

                City Tln = new City { Name = "Tallinn" };
                City Trt = new City { Name = "Tartu" };
                City Prn = new City { Name = "Parnu" };

                Hotel Hilton = new Hotel { Name = "Hilton Tallinn", StreetAddress = "Gonsiori 22" };
                Hotel Malibu = new Hotel { Name = "Malibu Parnu", StreetAddress = "Ranna tee 5" };
                Hotel Olympic = new Hotel { Name = "Olympic Tartu", StreetAddress = "Ylikooli 2" };



                Estonia.Cities.Add(Tln);
                Estonia.Cities.Add(Trt);
                Estonia.Cities.Add(Prn);

                Tln.Hotels.Add(Hilton);
                Prn.Hotels.Add(Malibu);
                Trt.Hotels.Add(Olympic);

                ctx.Countries.Add(Estonia);
                ctx.Cities.AddRange(Tln, Trt, Prn);
                ctx.Hotels.AddRange(Hilton, Malibu, Olympic);
                ctx.SaveChanges();

            }
        }
    }
}
