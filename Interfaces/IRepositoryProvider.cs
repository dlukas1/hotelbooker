﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IRepositoryProvider
    {
        TRepository GetCustomRepository<TRepository>();
        IRepository<TEntity> GetStandertRepository<TEntity>()
            where TEntity : class;
    }
}
