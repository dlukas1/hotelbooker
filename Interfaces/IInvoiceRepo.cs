﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IInvoiceRepo : IRepository<Invoice>
    {
    }
}
