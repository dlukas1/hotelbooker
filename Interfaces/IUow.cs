﻿using Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IUow
    {
        /// <summary>
        /// Atomic save into datastore
        /// <returns>Number of changes made</returns>
        /// </summary>
        int SaveChanges();

        IInvoiceRepo Invoices { get; } //will have special functionality
        IReservationRepo Reservations { get; }

        IRepository<Country> Countries { get; } //generic with no spexial func
        IRepository<City> Cities { get; }
        IRepository<Hotel> Hotels { get; }
        IRepository<Room> Rooms { get; }
        IRepository<User> Users { get; }



    }
}
