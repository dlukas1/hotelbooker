﻿using Domain;

namespace Interfaces
{
    public interface IHotelRepo : IRepository<Hotel>
    {
    }
}
