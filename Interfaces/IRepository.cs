﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IRepository<TentityType>
    {
        List<TentityType> All { get; }
        TentityType GetById(int id);
        void Add(TentityType entity);
        void Update(TentityType entity);
        void Delete(int id);
        void Delete(TentityType entity);
        int SaveChanges();
    }
}
