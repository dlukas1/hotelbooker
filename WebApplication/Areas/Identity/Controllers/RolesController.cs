﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DAL;
using Microsoft.AspNetCore.Identity;

namespace WebApplication.Areas.Identity.Controllers
{
    [Area("Identity")]
    public class RolesController : Controller
    {
        private readonly AppDbContext _context;

        public RolesController(AppDbContext ctx)
        {
            _context = ctx;
        }

        //GET
        public async Task<IActionResult> Index()
        {         
           return View(await _context.Roles.ToListAsync());
        }

        //GET: Identity/Roles/Details/4
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
                return NotFound();

            var aspNetUserRoles = await _context.Roles
                .SingleOrDefaultAsync(m => m.Id == id);
            if (aspNetUserRoles == null)
                return NotFound();
            return View(aspNetUserRoles);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task <IActionResult> Create(IdentityRole identityRole)
        {
            if (ModelState.IsValid)
            {
                _context.Add(identityRole);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(identityRole);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
                return NotFound();

            var role = _context.Roles.SingleOrDefault(m => m.Id == id);
            if (role == null)
                return NotFound();

            return View(role);
        } 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, IdentityRole identityRole)
        {
            if (id != identityRole.Id)
                return NotFound();

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(identityRole);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RoleExist(identityRole.Id))
                        return NotFound();
                    else { throw; }
                }
                return RedirectToAction("Index");
            }
            return View(identityRole);
        }

        //GET: Identity/Roles/Delete/3
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null) return NotFound();

            var role = await _context.Roles.SingleOrDefaultAsync(m => m.Id == id);
            if (role == null) return NotFound();

            return View(role);
        }

        //POST: Identity/Roles/Delete/3
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var role = await _context.Roles.SingleOrDefaultAsync(m => m.Id == id);
            _context.Roles.Remove(role);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool RoleExist(string id)
        {
            return _context.Roles.Any(m => m.Id == id);
        }
    }
}
