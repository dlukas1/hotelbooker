﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Controllers
{
    public class HotelsController : Controller
    {

        private readonly IUow _uow;
        public HotelsController(IUow uow)
        {
            _uow = uow;
        }

        public IActionResult Index()
        {
            var hotelsCount = _uow.Hotels.All.Count();
            ViewData["Message"] = $"There is {hotelsCount} hotels in DB";

            return View();
        }
    }
}