﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Invoices
    {
        public Invoices()
        {
            Reservations = new HashSet<Reservations>();
        }

        public int Id { get; set; }
        public double PriceTotal { get; set; }
        public int HotelId { get; set; }
        public int UserId { get; set; }
        public DateTime Issued { get; set; }

        public Hotels Hotel { get; set; }
        public Users User { get; set; }
        public ICollection<Reservations> Reservations { get; set; }
    }
}
