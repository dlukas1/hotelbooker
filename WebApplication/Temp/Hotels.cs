﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Hotels
    {
        public Hotels()
        {
            Invoices = new HashSet<Invoices>();
            Reservations = new HashSet<Reservations>();
            Rooms = new HashSet<Rooms>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string StreetAddress { get; set; }
        public int CityId { get; set; }

        public Cities City { get; set; }
        public ICollection<Invoices> Invoices { get; set; }
        public ICollection<Reservations> Reservations { get; set; }
        public ICollection<Rooms> Rooms { get; set; }
    }
}
