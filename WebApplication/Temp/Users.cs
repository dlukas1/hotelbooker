﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Users
    {
        public Users()
        {
            Invoices = new HashSet<Invoices>();
            Reservations = new HashSet<Reservations>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }

        public ICollection<Invoices> Invoices { get; set; }
        public ICollection<Reservations> Reservations { get; set; }
    }
}
