﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Countries
    {
        public Countries()
        {
            Cities = new HashSet<Cities>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Cities> Cities { get; set; }
    }
}
