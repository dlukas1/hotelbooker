﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Rooms
    {
        public Rooms()
        {
            Reservations = new HashSet<Reservations>();
        }

        public int Id { get; set; }
        public string RoomNumber { get; set; }
        public int PricePerNight { get; set; }
        public int HotelId { get; set; }

        public Hotels Hotel { get; set; }
        public ICollection<Reservations> Reservations { get; set; }
    }
}
