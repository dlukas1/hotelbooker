﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Reservations
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int HotelId { get; set; }
        public int RoomId { get; set; }
        public DateTime ReservationFrom { get; set; }
        public DateTime ReservationTo { get; set; }
        public int? InvoiceId { get; set; }

        public Hotels Hotel { get; set; }
        public Invoices Invoice { get; set; }
        public Rooms Room { get; set; }
        public Users User { get; set; }
    }
}
