﻿using System;
using System.Collections.Generic;

namespace WebApplication.temp
{
    public partial class Cities
    {
        public Cities()
        {
            Hotels = new HashSet<Hotels>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public Countries Country { get; set; }
        public ICollection<Hotels> Hotels { get; set; }
    }
}
